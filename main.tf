provider "aws" {
  #  region = "us-east-1"
  #  access_key = ""
  #  secret_key = ""
  #  token = ""
}

# variable "vpc_cider_block" {
#   description = "vpc cider block"
#   default = "10.0.10.0/24"
#   //type = string
#   type = list(string)
# }
# variable "cider_blocks" {
#   description = "cider blocks for vpc and subnets"
#   type = list(string)
# }
variable "cider_blocks" {
  description = "cider blocks for vpc and subnets"
  type = list(object({
    cider_block = string
    name = string
  }))
}
variable "environment" {
  description = "dev env"
}
resource "aws_vpc" "developement-vpc" {
  //cidr_block = "10.0.0.0/16"
  //cidr_block = var.vpc_cider_block
  //cidr_block = var.cider_blocks[0]
  cidr_block = var.cider_blocks[0].cider_block
  tags = {
    //Name = "developement"
    //Name = var.environment
    Name = var.cider_blocks[0].name
    vpc_env = "dev"
  }
}

variable "subnet_cidr_block" {
  description = "subnet cidr block"
}

resource "aws_subnet" "dev-subnet-1" {
  vpc_id = aws_vpc.developement-vpc.id
  //cidr_block = "10.0.10.0/24"
  cidr_block = var.subnet_cidr_block
  availability_zone = "us-east-1a"
  tags = {
    Name = "subnet-1-dev"
  }
}

data "aws_vpc" "existing_vpc" {
  default = true
}

variable avail_zone {
  #exported as an env var
}
resource "aws_subnet" "dev-subnet-2" {
  vpc_id = data.aws_vpc.existing_vpc.id
  cidr_block = "10.0.10.0/24"
  #availability_zone = "us-east-1a"
  availability_zone = var.avail_zone
  tags = {
    Name : "subnet-2-dev"
  }
}

output "dev-vpc-id" {
  value = aws_vpc.developement-vpc.id
}
output "dev-subnet-id" {
  value = aws_subnet.dev-subnet-1.id
}